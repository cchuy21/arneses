<header class='row'>
    <a href="/" class="alogo"><img class="logo" src="/static/images/PUBLICO/KSW2_LOGOARNESES.png" /></a>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img class="logob" src="/static/images/PUBLICO/KSW2_LOGOARNESES.png" /></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                <li><a href='/'>INICIO</a></li>
                <li><a href='/mision'>MISIÓN</a></li>
                <li><a href='/vision'>VISIÓN</a></li>
                <li><a href='/politica'>POLITICA DE CALIDAD</a></li>
                <li><a href='/vacantes'>VACANTES</a></li>
                <li class="seguridad"><a href='/seguridad'>SEGURIDAD-CONTRATISTAS</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<section class="container">
    <section class='row'>
        <section class='col-xs-12 col-sm-5 col-md-5'>
            <ul class="lmenu">
                <br />
                <br />
                <li><a href='/'>INICIO</a></li>
                <br />
                <li><a href='/mision'>MISIÓN</a></li>
                <br />
                <li><a href='/vision'>VISIÓN</a></li>
                <br />
                <li><a href='/politica'>POLITICA DE CALIDAD</a></li>
                <br />
                <li><a href='/vacantes'>VACANTES</a></li>
                <br />
                <li><a href='/seguridad'><button class="btn btn-proveedores">SEGURIDAD-CONTRATISTAS</button></a></li>
                <br />
            </ul>
        </section>
        <section class='col-xs-12 col-sm-7 col-md-7'>
            <ul class="smenu">
                <li><a id="tit" href='#'>PROVEEDORES</a></li>
                <br /><br /><br />
                <li><a href='https://portal.ksmex.com.mx/SG-Proveedores/empresa.php' target="_blank" >REGISTRO DE EMPRESA</a></li>
                <br /><br />
                <li><a href='https://portal.ksmex.com.mx/SG-Proveedores/login_registro.php' target="_blank">REGISTRO DE TRABAJADORES</a></li>
                <br /><br />
                <li><a href='https://portal.ksmex.com.mx/SG-Proveedores/login_trabajador.php' target="_blank">LOGIN Y CAPACITACIÓN DE TRABAJADOR</a></li>
                <br /><br />
                <li><a href='https://portal.ksmex.com.mx/SG-Proveedores/busqueda.php' target="_blank">MÓDULO DE MONITOREO</a></li>
                <br />
                <br />
                <li><a href='https://www.ksmex.com.mx/kys_cr/' target="_blank">CONTRARECIVOS</a></li>
                <br />
                <br />
            </ul>
        </section>
    </section>
</section>
<footer style='position: fixed; bottom: 0;'>
    <section class='row'>
        <section class="col-xs-12 col-sm-12" style='text-align:center; font-size:13px;'>
            <section class="col-xs-12 col-sm-12"> "DANDO SEGUIMIENTO A LA POLÍTICA DE LA COMPAÑÍA CONTRA  LA CORRUPCIÓN Y LAS MALAS PRACTICAS, CUALQUIER SITUACIÓN DENUNCIARLA".<br />AL TELÉFONO 910 06 00 EXT. 1236 O A LA CUENTA DE CORREO <a href="mailto:denuncia@ksmex.com.mx" style='color:white!important'>denuncia@ksmex.com.mx</a>
            </section>
        </section>
        <section class="col-xs-12" style='text-align:center; font-size:13px;'>
            <section class="col-xs-12">
                ⓒ Copyright 2019 Todos Los Derechos Reservados.
            </section>
        </section>
   </section>
</footer>





-->