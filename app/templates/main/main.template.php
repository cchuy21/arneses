<video width="100%" height="auto" autoplay="true" muted="muted" playsinline="" class="vdeo">
   <source src="#" type="video/mp4">
</video>
<header class='row'>
    <a href="/" class="alogo"><img class="logo" src="/static/images/PUBLICO/KSW2_LOGOARNESES.png" /></a>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/"><img class="logob" src="/static/images/PUBLICO/KSW2_LOGOARNESES.png" /></a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                <li class="active"><a href='/'>INICIO</a></li>
                <li><a href='/mision'>MISIÓN</a></li>
                <li><a href='/vision'>VISIÓN</a></li>
                <li><a href='/politica'>POLITICA DE CALIDAD</a></li>
                <li><a href='/vacantes'>VACANTES</a></li>
                <li class="seguridad"><a href='/seguridad'>SEGURIDAD-CONTRATISTAS</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<section class="container">
    <section class='row'>
        <img src="/static/images/PUBLICO/KSW2_PUNTOSDERECHA.png" class="izqu" style="position: fixed; right: 0px; top: 5%; width:20%; height:auto">
        <img src="/static/images/PUBLICO/KSW2_PUNTOSIZQUIERDA.png" class="dere" style="position: fixed; left: 0px; bottom: 5%; width:20%; height:auto">    
        <section class='col-xs-5'>
            <ul class="lmenu">
                <br />
                <br />
                <li class="active"><a href='/'>INICIO</a>&nbsp;&nbsp;<span class="glyphicon glyphicon-minus" aria-hidden="true"></span></li>
                <br />
                <li><a href='/mision'>MISIÓN</a></li>
                <br />
                <li><a href='/vision'>VISIÓN</a></li>
                <br />
                <li><a href='/politica'>POLITICA DE CALIDAD</a></li>
                <br />
                <li><a href='/vacantes'>VACANTES</a></li>
                <br />
                <li><a href='/seguridad'><button class="btn btn-proveedores">SEGURIDAD-CONTRATISTAS</button></a></li>
                <br />
            </ul>
        </section>
        <section class='col-xs-7'>
        </section>
    </section>
</section>
</section>
<footer style='position: fixed; bottom: 0;'>
    <section class='row'>
        <section class="col-xs-12 col-sm-12" style='text-align:center; font-size:13px;'>
            <section class="col-xs-12 col-sm-12"> "DANDO SEGUIMIENTO A LA POLÍTICA DE LA COMPAÑÍA CONTRA  LA CORRUPCIÓN Y LAS MALAS PRACTICAS, CUALQUIER SITUACIÓN DENUNCIARLA".<br />AL TELÉFONO 910 06 00 EXT. 1236 O A LA CUENTA DE CORREO <a href="mailto:denuncia@ksmex.com.mx" style='color:white!important'>denuncia@ksmex.com.mx</a>
            </section>
        </section>
        <section class="col-xs-12" style='text-align:center; font-size:13px;'>
            <section class="col-xs-12">
                ⓒ Copyright 2019 Todos Los Derechos Reservados.
            </section>
        </section>
   </section>
</footer>