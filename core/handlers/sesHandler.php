<?php

class sesHandler{
    protected $table = 'sessions';
    public function created(){
    	//aqui es donde debemos crear la sesion para los usuarios del sistema
    }
    public function open() {
    	     return true;
    }

    public function close() {
    	    return true;
    }

    public function read($id) {
    	
        $session = Session::find_by_id($id);
        if( $session ) {
        	//para iniciar la sesión, envia los datos si la sesión es correcta
            print_r($session->data);
            return $session->data;
        } else {
            return false;
        }
    }

    public function write($id, $data) {
    	//
        $session = Session::find_by_id($id);
        if( $session ){
            $session->data = $data;
            $session->save();
            return true;
        } else {
        	//se asigna datos a una sesión y es creada
            $session = Session::create(
                array(
                    "id" => $id,
                    "data" => $data
                )
            );
        }
        return false;
    }
	//se elimina una sesion registrada en la base de datos
    public function destroy($id) {
        $session = Session::find_by_id($id);
        if( $session )
            $session->delete();
        return true;
    }

    public function gc($max) {
    	//intval- obtiene el valor entero de una variable
        $query = sprintf("DELETE FROM %s WHERE `created_at` < '%s'", $this->table, time() - intval($max));
        return dbDriver::execQuery($query);

    }

}