<?php
class templateDriver extends driverBase{
    private static $type = 'html';
    private static $content = null;
    private static $data = array();
	
    public static function setContent($content = null) {
        if( $content === null )
            return false;
        self::$content = $content;
    }
    public static function setData($k = null, $d = null) {
        if($k === null)
            return false;
        self::$data[$k] = $d;
    }
    public static function getData($k = null) {
        if( $k === null )
            return self::$data;
        return isset(self::$data[$k]) ? self::$data[$k] : null;
    }
    //incluye un template si esque existe sino manda mensaje de error
    public static function render($content = false, $data = null, $template = 'index') {
        if( $data )
            self::setData("content", $data);
        
        templateDriver::setContent($content ? $content : configDriver::defaultContent());
        //incluye los archivos de template, si es que existen
        if( file_exists("../app/templates/".$template.".template.php") ) 
            include("../app/templates/".$template.".template.php");
        else
            die("No existe el template especificado");
    }
    
    public static function content(){
        self::renderSection(self::$content);
    }
    
    public static function renderRaw($sect = null, $data = null){
	self::renderSection($sect, $data);
    }
	//convierte los dias de ingles a español
    public static function renderSection($sect = null, $data = null, $overwrite = true){
		if(!isset($_SESSION)) 
		{ 
			ob_start(); session_start(); ob_end_clean();
		} 
	$section = "../app/templates";
        $key = null;
		//separa los valores de sect por medio de los puntos
	foreach (explode('.', $sect) as $value) {
            $section = $section . "/" . $value;
            $key = $value;
	}
        if( $data !== null ) {
            if( !$overwrite ) {
                $i = 2;
                while(isset(self::$data[$key]))
                    $key = $key.$i++;
            }
            self::setData($key, $data);
        }
		//verifica si el archivo .php existe en el sistema
        if( file_exists($section.".php") ) 
            $section = $section.".php"; 
        elseif (file_exists($section.".template.php")) //busca el archivo en template
            $section = $section.".template.php";
        else //sino encuentra el archivo solicitado
            die("No existe la sección especificada"); 
        include($section);
    }
    
}
