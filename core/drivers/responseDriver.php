<?php

class responseDriver extends driverBase {
	
        public static $type = "D";
        public static $title = null;
        public static $description = null;
        public static $code = null;
        public static $data = null;
        
	private static function Json( $data ) { 
            if (function_exists('json_encode'))
                die(json_encode($data));
	    if( is_array($data) || is_object($data) ) {
	    	//si la variable es un array, array_keys=devuelv e todas las palabras claves de un array  range=un array que contiene ciertos elementos
	        $islist = is_array($data) && ( empty($data) || array_keys($data) === range(0,count($data)-1) ); 
	        
	        if( $islist ) { //implode=manda todos los elementos de un array con un corchete abierto[
	            $json = '[' . implode(',', array_map('__json_encode', $data) ) . ']'; 
	        } else { 
	            $items = Array(); 
	            foreach( $data as $key => $value ) { 
	                $items[] = __json_encode("$key") . ':' . __json_encode($value); 
	            } 
	            $json = '{' . implode(',', $items) . '}'; 
	        } 
	    } elseif( is_string($data) ) { 
	        # Escape non-printable or Non-ASCII characters. 
	        # I also put the \\ character first, as suggested in comments on the 'addclashes' page. 
	        $string = '"' . addcslashes($data, "\\\"\n\r\t/" . chr(8) . chr(12)) . '"'; 
			$json    = ''; 
	        $len    = strlen($string); 
	        # Convert UTF-8 to Hexadecimal Codepoints. 
	        for( $i = 0; $i < $len; $i++ ) { 
	            
	            $char = $string[$i]; 
	            $c1 = ord($char); 
	            
	            # Single byte; 
	            if( $c1 <128 ) { //devuelve la variable con un formato definido
	                $json .= ($c1 > 31) ? $char : sprintf("\\u%04x", $c1); 
	                continue; 
	            } 
	            
	            # Double byte 
	            //ord=devuelve el valor ASCII de un valor
	            $c2 = ord($string[++$i]); 
	            if ( ($c1 & 32) === 0 ) { 
	                $json .= sprintf("\\u%04x", ($c1 - 192) * 64 + $c2 - 128); 
	                continue; 
	            } 
	            
	            # Triple 
	            //ord=devuelve el valor ASCII de un valor
	            $c3 = ord($string[++$i]); 
	            if( ($c1 & 16) === 0 ) { 
	                $json .= sprintf("\\u%04x", (($c1 - 224) <<12) + (($c2 - 128) << 6) + ($c3 - 128)); 
	                continue; 
	            } 
	                
	            # Quadruple 
	            //ord=devuelve el valor ASCII de un valor
	            $c4 = ord($string[++$i]); 
	            if( ($c1 & 8 ) === 0 ) { 
	                $u = (($c1 & 15) << 2) + (($c2>>4) & 3) - 1; 
	            
	                $w1 = (54<<10) + ($u<<6) + (($c2 & 15) << 2) + (($c3>>4) & 3); 
	                $w2 = (55<<10) + (($c3 & 15)<<6) + ($c4-128); 
	                $json .= sprintf("\\u%04x\\u%04x", $w1, $w2); 
	            } 
	        } 
	    } else { 
	        # int, floats, bools, null 
	        //var-export=devuelve la representación de la variable dada y la convierte a minusculas
	        $json = strtolower(var_export( $data, true )); 
	    } 
	    die($json); 
	} 
        
        private static function arrayToXml($arr) {
            $xml = '';
            foreach($arr  as $key => $val) {
            	//comprueba que la variable no sea un número
                if(!is_numeric($key))
                    $xml .= "<{$key}>";
				//verifica si una variable es un array
                if( is_array($val) ) {
                    $xml .= self::arrayToXml($val);
                } else {
                    $xml .= $val;
                }
				//verifica si la variable no es un número
                if(!is_numeric($key))
                    $xml .= "</{$key}>";
            }
            return $xml;
        }
        
        private static function xmlDispatcher(){
        	//define datos de un encabezado 
            header('Content-type: text/xml');
            $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
            $xml .= "<Message>";
            $xml .= self::arrayToXml(self::$data);
            $xml .= "</Message>";
            die($xml);
        }
        
        private static function jsonDispatcher(){
            self::Json(self::$data);
        }
        
        public static function dispatch($type = 'D', $title = '', $description = '') {
            switch ($type) {
                case 'D':
                    self::$data = $title;
                    break;
                case 'M':
                    self::$data = array("mensaje" => $title.":".$description);
                    break;
                case 'E':
                    self::$data = array("error" => $title.":".$description);
                    break;
                default:
                    break;
            }
            if(inputDriver::getVar("isMobile") !== null) {
            	//manda a llamar a la función que especifica el encabezado del mensaje
                self::xmlDispatcher();
            } else {
                self::jsonDispatcher();
            }
        }
}
